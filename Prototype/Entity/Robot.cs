﻿namespace Prototype.Entity
{
    public class Robot : IMyCloneable<Robot>,ICloneable
    {
        public int Weight { get; set; }
        public int Speed { get; set; }
        public bool IsFly { get; set; }
        public bool IsSweem { get; set; }
        public bool IsAtack { get; set; }

        public Robot()
        {
            Weight = 0;
            Speed = 0;
            IsFly = false;
            IsSweem = false;
            IsAtack = false;
        }

        public Robot(int weight, int speed, bool isFly, bool isSweem, bool isAtack)
        {
            Weight = weight;
            Speed = speed;
            IsFly = isFly;
            IsSweem = isSweem;
            IsAtack = isAtack;
        }

        public Robot Copy()
        {
            return new Robot(Weight, Speed, IsFly, IsSweem, IsAtack);
        }

        public object Clone()
        {
            return Copy();
        }

        public override string ToString()
        {
            return $"Weight:\t{Weight},\n" +
                $"Speed:\t{Speed},\n" +
                $"IsFly:\t{IsFly},\n" +
                $"IsSweem:\t{IsSweem},\n" +
                $"IsAtack:\t{IsAtack}.";
        }
    }
}
