﻿namespace Prototype.Entity
{
    public class TransportMilitaryRobot : MilitaryRobot, IMyCloneable<TransportMilitaryRobot>, ICloneable
    {
        public int WeightCapaacity { get; set; }

        public TransportMilitaryRobot()
        {
            WeightCapaacity = 0;
        }

        public TransportMilitaryRobot(Robot robot,int damage, int weightCapaacity)
        {
            Weight = robot.Weight;
            Speed = robot.Speed;
            IsFly = robot.IsFly;
            IsSweem = robot.IsSweem;
            IsAtack = true;
            Damage = damage;
            WeightCapaacity = weightCapaacity;
        }

        public TransportMilitaryRobot(MilitaryRobot militaryRobot, int weightCapaacity)
        {
            Weight = militaryRobot.Weight;
            Speed = militaryRobot.Speed;
            IsFly = militaryRobot.IsFly;
            IsSweem = militaryRobot.IsSweem;
            IsAtack = militaryRobot.IsAtack;
            Damage = militaryRobot.Damage;
            WeightCapaacity = weightCapaacity;
        }

        public TransportMilitaryRobot(int weight, int speed, bool isFly, bool isSweem, int damage, int weightCapaacity)
        {
            Weight = weight;
            Speed = speed;
            IsFly = isFly;
            IsSweem = isSweem;
            IsAtack = true;
            Damage = damage;
            WeightCapaacity = weightCapaacity;
        }

        public new TransportMilitaryRobot Copy()
        {
            return new TransportMilitaryRobot(Weight, Speed, IsFly, IsSweem, Damage, WeightCapaacity);
        }

        public override string ToString()
        {
            return $"Weight:\t{Weight},\n" +
                $"Speed:\t{Speed},\n" +
                $"IsFly:\t{IsFly},\n" +
                $"IsSweem:\t{IsSweem},\n" +
                $"IsAtack:\t{IsAtack},\n" +
                $"Damage:\t{Damage},\n" +
                $"WeightCapaacity:\t{WeightCapaacity}.";
        }
    }
}
