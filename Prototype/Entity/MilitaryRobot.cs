﻿namespace Prototype.Entity
{
    public class MilitaryRobot : Robot, IMyCloneable<MilitaryRobot>, ICloneable
    {
        public int Damage { get; set; }

        public MilitaryRobot()
        {
            IsAtack = true;
            Damage = 0;
        }

        public MilitaryRobot(Robot robot, int damage)
        {
            Weight = robot.Weight;
            Speed = robot.Speed;
            IsFly = robot.IsFly;
            IsSweem = robot.IsSweem;
            IsAtack = true;
            Damage = damage;
        }
        
        public MilitaryRobot(int weight, int speed, bool isFly, bool isSweem, int damage)
        {
            Weight=weight;
            Speed=speed;
            IsFly=isFly;
            IsSweem=isSweem;
            IsAtack = true;
            Damage = damage;
        }

        public new MilitaryRobot Copy()
        {
            return new MilitaryRobot(Weight,Speed,IsFly,IsSweem,Damage);
        }

        public override string ToString()
        {
            return $"Weight:\t{Weight},\n" +
                $"Speed:\t{Speed},\n" +
                $"IsFly:\t{IsFly},\n" +
                $"IsSweem:\t{IsSweem},\n" +
                $"IsAtack:\t{IsAtack},\n" +
                $"Damage:\t{Damage}.";
        }
    }
}
