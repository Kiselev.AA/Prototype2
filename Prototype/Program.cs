﻿using Prototype.Entity;

namespace Prototype
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome!");
            Console.WriteLine();

            var robo1 = new TransportMilitaryRobot(1, 2, true, true, 3, 4);
            PrintRobot("ROBO_1", robo1);
            var clone1 = robo1.Clone(); 
            PrintRobot("Clone ROBO_1", clone1);


            var robo2 = new TransportMilitaryRobot(5, 6, true, true, 7, 8);
            PrintRobot("ROBO_2", robo2);
            var copy2 = robo2.Copy();
            PrintRobot("Copy ROBO_2", copy2);
        }

        private static void PrintRobot(string name, object obj)
        {
            Console.WriteLine(name);
            Console.WriteLine(obj.ToString());
            Console.WriteLine();
        }
    }
}