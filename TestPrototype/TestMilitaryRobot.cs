using Prototype.Entity;
namespace TestPrototype
{
    [TestClass]
    public class TestMilitaryRobot
    {
        [TestMethod]
        public void TestClone()
        {
            var original = new MilitaryRobot(10, 20, false, false, 15);
            var clone = (MilitaryRobot)original.Clone();
            Assert.AreEqual(original.Weight, clone.Weight);
            Assert.AreEqual(original.Speed, clone.Speed);
            Assert.AreEqual(original.IsFly, clone.IsFly);
            Assert.AreEqual(original.IsSweem, clone.IsSweem);
            Assert.AreEqual(original.IsAtack, clone.IsAtack);
            Assert.AreEqual(original.Damage, clone.Damage);
            Assert.AreEqual(original.ToString(), clone.ToString());
        }

        [TestMethod]
        public void TestCopy()
        {
            var original = new MilitaryRobot(10, 20, false, false, 15);
            var copy = original.Copy();
            Assert.AreEqual(original.Weight, copy.Weight);
            Assert.AreEqual(original.Speed, copy.Speed);
            Assert.AreEqual(original.IsFly, copy.IsFly);
            Assert.AreEqual(original.IsSweem, copy.IsSweem);
            Assert.AreEqual(original.IsAtack, copy.IsAtack);
            Assert.AreEqual(original.Damage, copy.Damage);
            Assert.AreEqual(original.ToString(), copy.ToString());
        }
    }
}